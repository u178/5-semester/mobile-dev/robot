// 1. реализовать функции поворота turnLeft() turnRight()
// 2. написать функцию moveTo(toX, toY) перемещающую робота в требуемую точку

enum class Direction() {
    UP,
    RIGHT,
    DOWN,
    LEFT
}
data class Robot(
    private var x: Int,
    private var y: Int,
    private var direction: Direction
    ) {

    fun stepForward() {
        when (direction) {
            Direction.UP -> y += 1
            Direction.DOWN -> y -= 1
            Direction.LEFT -> x -= 1
            Direction.RIGHT -> x += 1
        }
    }

    override fun toString(): String {
        return "x: $x, y: $y, dir: $direction"
    }

    fun turnLeft() {
        direction = when (direction) {
            Direction.UP -> Direction.RIGHT
            Direction.DOWN -> Direction.LEFT
            Direction.LEFT -> Direction.UP
            Direction.RIGHT -> Direction.DOWN
        }
    }

    fun turnRight() {
        direction = when (direction) {
            Direction.UP -> Direction.LEFT
            Direction.DOWN -> Direction.RIGHT
            Direction.LEFT -> Direction.DOWN
            Direction.RIGHT -> Direction.UP
        }
    }

    fun moveTo(toX: Int, toY: Int) {
//        println("x: $x, y: $y, dir: $direction")

        if (toX > x) {
            while (direction != Direction.RIGHT) {
                turnRight()
            }
        }
//        println("x: $x, y: $y, dir: $direction")

        if (toX < x) {
            while (direction != Direction.LEFT) {
                turnRight()
            }
        }

        while (toX != x) {
            stepForward()
        }


        if (toY > y) {
            while (direction != Direction.UP) {
                turnRight()
            }
        }

        if (toY < y) {
            while (direction != Direction.DOWN) {
                turnRight()
            }
        }

        while (toY != y) {
            stepForward()
        }
    }
}

fun main() {
    val r = Robot(1,1, Direction.RIGHT)
    print("start location: ")
    r.stepForward()
    r.moveTo(10, -4)
    print("end location: ")
    println(r)
}